package com.alvarobrm.poc.resilience4j;

import com.alvarobrm.poc.resilience4j.service.RetryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AspectTest {


    @Autowired
    private RetryService service ;

    @Test(expected = RuntimeException.class)
    public void test (){
        service.retry();
    }
}
