package com.alvarobrm.poc.resilience4j.service;

import com.alvarobrm.poc.resilience4j.config.Retryable;
import org.springframework.stereotype.Service;

@Service
public class RetryService {

    @Retryable(maxAttempts = 10, multiplier = 1.2)
    public void retry (){
        System.out.println("hello");
        throw new RuntimeException("e");
    }
}
