package com.alvarobrm.poc.resilience4j.config;

import io.github.resilience4j.core.IntervalFunction;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;
import io.vavr.CheckedFunction0;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RetryableAspect {


    @Around("@annotation(retryable)")
    public Object retryable(ProceedingJoinPoint joinPoint, Retryable retryable) throws Throwable {

        IntervalFunction intervalFunction = IntervalFunction
                .ofExponentialBackoff(retryable.initialWaitDuration(), retryable.multiplier());
        RetryConfig config = RetryConfig.custom()
                .maxAttempts(retryable.maxAttempts())
                .intervalFunction(intervalFunction)
                .build();
        RetryRegistry retryRegistry = RetryRegistry.of(config);

        Retry retry = retryRegistry.retry("name1");

        CheckedFunction0<Object> retryableSupplier = Retry.decorateCheckedSupplier(retry, joinPoint::proceed);

        return retryableSupplier.apply();
    }


}
