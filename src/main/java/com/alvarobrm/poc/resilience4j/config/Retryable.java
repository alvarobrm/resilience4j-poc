package com.alvarobrm.poc.resilience4j.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Retryable {

    int maxAttempts () default 3;

    long initialWaitDuration () default 1000;

    double multiplier () default 1;

}
