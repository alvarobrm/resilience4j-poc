package com.alvarobrm.poc.resilience4j;

import com.alvarobrm.poc.resilience4j.service.RetryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


@SpringBootApplication
public class Resilience4jApplication {



    public static void main(String[] args) {
        SpringApplication.run(Resilience4jApplication.class, args);
    }

}
